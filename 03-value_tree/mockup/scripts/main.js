
var base_url = "http://92.51.132.110/~andre/memory/wiki/" // use trailing slash plz
var apiurl = base_url + "api.php";

$.ajaxSetup({
  //xhrFields : { withCredentials : true },
  // async: false
});

function build_url(querydict) {
  if ( !querydict ) { return apiurl }
  var url = apiurl + '?';
  $.each(querydict, function(key, value) {
    if ( url.substr(-1) != '?') {
      url += '&';
    }
    url += key + '=' + encodeURIComponent(value); 
  });
  return url
}
  // var qurl = build_url({ format: 'json', action: 'query', 'titles': 'Property:Person' });

function search_text (t, searchtype = 'title') {
  var qurl = build_url({ format: 'json', 
    action: 'query', 
    list: 'search',
    srsearch: t,
    srlimit: 50,
    srwhat: 'title'
  });
  $.get(qurl, function (data, e) {
    if ( e != "success" ) { 
      console.log("Errors found:"); 
      console.log(e);
    }
    console.log(data);
  });
}


function add_image (f, prop) {
  // api.php?action=query&titles=File:Albert%20Einstein%20Head.jpg&prop=imageinfo&&iiprop=url&iiurlwidth=220
  var qurl = build_url({ format: 'json', 
    action: 'query', 
    titles: f,
    prop: 'imageinfo',
    iiprop: 'url',
    iiurlheight: 50
  });
  $.get(qurl, function (data, e) {
    if ( e != "success" ) { 
      console.log("Errors found:"); 
      console.log(e);
    }
    // console.log(data);
    var imginfo = data.query.pages[Object.keys(data.query.pages)[0]].imageinfo[0]
    $('#' + prop + '-images > ul').append('<li><a href="' + imginfo.descriptionurl + '"><img src="' + imginfo.thumburl + '" alt="' + f + '"></a></li>');
  });
}



function add_to_results(data, prop) {
  //Array.prototype.push.apply(results, data);
  $.each(data.query.results, function(idx, value) {
    // console.log(value);
    if (value.namespace == 0) {
      console.log(value);
      $('#' + prop + '-pages > ul').append('<li><h5><a href="' + value.fullurl + '">' + value.fulltext + '</a></h5></li>');
    } else if (value.namespace == 6) {
      add_image(value.fulltext, prop);
    } else {
      console.log("Invalid namespace!");
      console.log(value);
    }
  });
}


function make_ask_query(q, property, offset) {
  // default offset should be 0
  offset = typeof offset !== 'undefined' ? offset : 0;
  var qurl = build_url({ format: 'json', 
    action: 'askargs', 
    conditions: q, 
    parameters: 'offset=' + offset
  });
  $.get(qurl, function (data, e) {
    // console.log(qurl);
    if ( e != "success" ) { 
      console.log("Errors found:"); 
      console.log(e);
    }
    add_to_results(data, property);
    if (data['query-continue-offset']) {
      make_ask_query(q, property, data['query-continue-offset']);
    }
  });
}

function get_pages_with_property(p, callback) {
  var result = make_ask_query(p + '::+', p);
}

$(document).ready(function(){
  console.log('Querying URL');
  // search_text('Person');
  $.each(['person', 'object', 'place', 'date', 'medium', 'register', 'creatures'], function(idx, prop) {
    $("#container").append('<div id="' + prop + '" class="prop row"></div>');
    $("#" + prop).append('<div class="prop-title small-2 columns"><h4>' + prop + '</h4></div>');
    $("#" + prop).append('<div id="' + prop +'-pages" class="page-list small-5 columns"><ul class="no-bullet"></ul></div>');
    $("#" + prop).append('<div id="' + prop +'-images" class="image-list small-5 columns"><ul class="small-block-grid small-block-grid-10"></ul></div>');
    get_pages_with_property(prop);
  });
});


