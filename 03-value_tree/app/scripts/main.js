
var base_url = 'http://37.218.246.155/warpweft-wiki/'; // use trailing slash plz
var apiurl = base_url + "api.php";


// http://stackoverflow.com/a/1026087
String.prototype.capitalize = function() {
  return this.charAt(0).toUpperCase() + this.slice(1);
}


function build_url(querydict) {
  // var qurl = build_url({ format: 'json', action: 'query', 'titles': 'Property:Person' });
  if ( !querydict ) { return apiurl }
  var url = apiurl + '?';
  $.each(querydict, function(key, value) {
    if ( url.substr(-1) != '?') {
      url += '&';
    }
    url += key + '=' + encodeURIComponent(value); 
  });
  return url
}

function slugify(s) {
  return s.toLowerCase().replace(/ /g, '-').replace("'", "").replace("è", "e").replace(".", "");
}


function fill_related_links(prop, value, offset) {
  // api.php?format=json&action=askargs&conditions=person%3A%3A%2B&printouts=Person
  offset = typeof offset !== 'undefined' ? offset : 0;
  var prop_slug = slugify(prop);
  var value_slug = slugify(value);
  var qurl = build_url({ format: 'json', 
    action: 'askargs', 
    conditions: prop + '::' + value, 
    parameters: 'offset=' + offset
  });
  $.get(qurl, function (data, e) {
    if ( e != "success" ) { 
      console.log("Errors found in ask query:"); 
      console.log(e);
    } else {
      var count = 1;
      $.each(data.query.results, function(i, r) {
        $('#property-' + prop_slug + '-value-' + value_slug + '-list ul.related-items').append('<li id="related-' + prop_slug + '-' + value_slug + '-' + count + '">');
        $('#related-' + prop_slug + '-' + value_slug + '-' + count).append('<a href="' + r.fullurl + '">' + r.fulltext + '</a>');

        // previews
        add_preview_content(prop, value, count, r.fulltext);

        count++;
      }); 
    }
    if (data['query-continue-offset']) {
      get_values_for_property(prop, value, data['query-continue-offset']);
    }
  });
}



function add_preview_content(prop, value, count, title) {
  var prop_slug = slugify(prop);
  var value_slug = slugify(value);
  var $prev = $('<div id="preview-' + prop_slug + '-' + value_slug + '-' + count + '" class="page-preview"></div>').appendTo('#property-' + prop_slug + '-value-' + value_slug + '-list .preview-container');
  $prev.hide();

  if (title.startsWith('File:')) {
    var qurl = build_url({ 
      format: 'json', 
      action: 'query', 
      titles: title,
      prop: 'imageinfo',
      iiprop: 'url',
      iiurlheight: 250
    });
    $.get(qurl, function (data, e) {
      if ( e != "success" ) { 
        console.log("Errors found in add_image:"); 
        console.log(e);
      }
      var imginfo = data.query.pages[Object.keys(data.query.pages)[0]].imageinfo[0]
      $prev.append('<img src="' + imginfo.thumburl + '" alt="' + title + '">');

    });
  } else {
    var qurl = build_url({ 
      format: 'json', 
      action: 'parse', 
      page: title,
    });
    $.get(qurl, function (data, e) {
      if ( e != "success" ) { 
        console.log("Errors found in add_image:"); 
        console.log(e);
      }
      var contents = data.parse.text['*'];
      $prev.append(contents);
    });
  }

  var prev_id = $prev[0].id
  var related_id = $prev[0].id.replace('preview-', 'related-');
  var $rel = $("#" + related_id + ' a');
  $rel.hover(function() {
    if (!$($prev).is(':visible')) {
      $prev.show('fast');     
    }
  });
  $rel.on('mouseout', function() {
    if ($prev.is(':visible')) {
      $prev.hide();     
    }
  });
}


function parse_results_for_values(prop, results) {
  $.each(results, function(idx, result) {
    var prop_slug = slugify(prop);

    $.each(result.printouts[prop.capitalize()], function(i, v) {
      if (v.fulltext) {
        var value = v.fulltext;
        prop = prop.toLowerCase();
        var value_slug = slugify(value);
        // create list of values
        if (!$('li#property-' + prop_slug + '-value-' + value_slug).length) {
          var $li = $('<li id="property-' + prop_slug + '-value-' + value_slug + '"></li>').appendTo('#property-' + prop_slug + ' ul.value-items');
          $li.append('<a href="#/">' + value + '</a>');

          // fill the hidden value containers
          var $valuebox = $('<div id="property-' + prop_slug + '-value-' + value_slug + '-list" class="property-value-list row"></div>').appendTo('#property-' + prop_slug + ' #value-list-container');

          $valuebox.append('<p class="utility small-12 columns"><a href="' + v.fullurl + '" target="_blank">Go to <strong>' + value + '</strong> →</a></p>');
          $valuebox.append('<ul class="related-items no-bullet small-6 columns"></ul>');
          $valuebox.append('<div class="preview-container small-6 columns"></div>');
          fill_related_links(prop, value);  

          // $(document).foundation('equalizer', 'reflow');

          $('#property-' + prop + '-value-' + value_slug).click(function(e) {
              e.preventDefault();
              var li_id = $(this)[0].id;
              // close open property-list if you click in another property or place
              $('.property-value-list').hide();
              // remove class active if you click in another property
              $('.active').not(this).removeClass('active');
              $(this).toggleClass('active');
              $('#'+ li_id + '-list').toggle('fast');
              return false;
          });
        }
      } 
    });
  });
}


function get_values_for_property(prop, offset) {
  // api.php?format=json&action=askargs&conditions=person%3A%3A%2B&printouts=Person
  offset = typeof offset !== 'undefined' ? offset : 0;
  var qurl = build_url({ format: 'json', 
    action: 'askargs', 
    conditions: prop + '::+', 
    printouts: prop.capitalize(),
    parameters: 'offset=' + offset
  });
  $.get(qurl, function (data, e) {
    if ( e != "success" ) { 
      console.log("Errors found in ask query:"); 
      console.log(e);
    } else {
      parse_results_for_values(prop, data.query.results);
    }
    if (data['query-continue-offset']) {
      get_values_for_property(prop, data['query-continue-offset']);
    }
  });
}


function make_ask_query(q, property, offset) {
  // default offset should be 0
  offset = typeof offset !== 'undefined' ? offset : 0;
  var qurl = build_url({ format: 'json', 
    action: 'askargs', 
    conditions: q, 
    printouts: property.capitalize(),
    parameters: 'offset=' + offset
  });
  var results = [];
  $.get(qurl, function (data, e) {
    if ( e != "success" ) { 
      console.log("Errors found in ask query:"); 
      console.log(e);
    } else {
      add_to_results(data, property);
    }
      // results.push.apply(results, more_results);

    if (data['query-continue-offset']) {
      var more_results = make_ask_query(q, property, data['query-continue-offset']);
      results.push.apply(results, more_results);
    }
  });
}

$(document).ajaxStop(function() {
  $('#content-loader').hide('slow');
});

$(document).ready(function(){
  $.each(['person', 'object', 'place', 'date', 'medium', 'register', 'creatures'], function(idx, prop) {
    // Add property section
    $('<div id="property-' + prop + '" class="property row"></div>').appendTo("#main-container");
    $('<div class="small-2 columns"></div>').appendTo('#property-' + prop);
    $('<div class="property-name"><h3>' + prop.capitalize() + '</h3></div>').appendTo('#property-' + prop + ' div');

    $('<div id="property-' + prop + '-list" class="property-list small-3 columns"></div>').appendTo('#property-' + prop);
    $('<div id="value-list-container" class="small-7 end columns"></div>').appendTo('#property-' + prop);
    $('<ul class="no-bullet value-items"></ul>').appendTo('#property-' + prop + '-list');
    get_values_for_property(prop);

    // este js serve para alinhar verticalmente uma property-x com a respectiva property-value-list
    $('.property-list li').click(function(e) {
      var li_id = $(this)[0].id;
      var offset = li_id.offset();
      var topMargin = offset.top - li_id.outerHeight(true);
      $('#'+ li_id + '-list').toggle('fast').css('margin-top', topMargin + 'px');
      $('#'+ li_id).toggleClass('active');
      return false;      
    });
    
  });
});
