var base_domain = 'http://37.218.246.155';
var base_url = base_domain + '/warpweft-wiki/'; // use trailing slash plz
var apiurl = base_url + 'api.php';

var initialProperty = 'Person';
var initialValue = 'Gisèle';
var propertiesToIgnore = ['File', 'Page_has_default_form', 'Properties', 'Property', 'Category', 'Creature', 'Location'];
var propertiesToInclude = ['TOC', 'Date', 'Theme', 'Author', 'Treasured Object', 'Recurring Garment'];

function build_url(querydict) {
  // helper function to construct the URL for the API query
  // var qurl = build_url({ format: 'json', action: 'query', 'titles': 'Property:Person' });
  if ( !querydict ) { return apiurl; }
  var url = apiurl + '?';
  // add 'origin' parameter: https://www.mediawiki.org/wiki/API:Cross-site_requests
  url += 'origin=' + encodeURIComponent('*'); 
  url += '&format=json';
  $.each(querydict, function(key, value) {
    if ( url.substr(-1) != '?') {
      url += '&';
    }
    url += key + '=' + encodeURIComponent(value); 
  });
  return url;
}

function title_compare(a,b) {
  // https://stackoverflow.com/a/1129270/122400
  if (a.title < b.title)
    return -1;
  if (a.title > b.title)
    return 1;
  return 0;
}

function get_page_html(detailData) {
  var text = detailData.text['*'];
  text = text.replace(/\n|\r/g, '')
    .replace(/<p><br \/><\/p>/ig, '');
  var $nodes = $.parseHTML(text);
  var html = '';
  $.each($nodes, function(i, el) {
    // omit div.properties
    if (el.className != 'properties') {
      if (el.innerHTML) {
        if ($(el).find('img').length) {
          $(el).find('img').attr('src', function(i, val) {
            // add domain to image sources
            return base_domain + val;
          });
          el = $(el)[0];
        }
        html += el.innerHTML;
      }
    }
  });
  return html;
}

var router = new VueRouter();

var vm = new Vue({
  el: '#app',
  name: 'imageQuilt',
  router: router,
  http: {
    headers: {
      Authorization: 'Basic ZWRpdG9yczpHaXNlbGU='
    }
  },
  // delimiters: ['${', '}'],
  data: {
    status: '',
    results: [],
    pages: [],
    properties: [],
    activeValues: [],
    activeProperty: 'Loading',
    selectedProperty: '',
    selectedValue: '',
    detail: {
      title: '',
      imageURL: '',
      content: '',
      preview: '',
      properties: [],
      backlinks: [],
      imagelinks: []
    }
  },


  ready: function() {
    $(this.$el).foundation();
  },

  mounted: function() {
    // stop video when closing sidebar
    $(document).on(
      'closed.zf.reveal', '[data-reveal]', function () {
        if ($('#detail-video').length) {
          $('#detail-video')[0].pause();
          console.log('video closed');
        }
      }
    );

    // init vars
    this.selectedPage = null;
    this.nextPage = null;
    this.previousPage = null;
    this.getAllProperties();

    // check if URL has query string and set selected prop/value
    if (this.$route.query.value) {
      this.selectedProperty = this.$route.query.prop;
      this.selectedValue = this.$route.query.value;
    } else {
      this.selectedProperty = initialProperty;
      this.selectedValue = initialValue;
    }
    this.selectProperty(this.selectedProperty);
    this.selectValue(this.selectedProperty, this.selectedValue);
  },

  methods: {
    selectValue: function(prop, value, offset) {
      this.selectedProperty = prop;
      this.selectedValue = value;

      this.status = 'Loading images...';
      if (this.selectedProperty != prop) {
        this.selectProperty(prop);
      }
      if (!offset) {
        this.results = [];
        this.pages = [];
        offset = 0;
      }

      var qurl = build_url({
        action: 'askargs', 
        conditions: prop + '::' + value,
        parameters: 'offset=' + offset
      });

      this.$http.get(qurl).then(function (response) {
        var r = JSON.parse(response.bodyText);
        var results = r.query.results; 

        var tqurl = build_url({ 
          action: 'query', 
          titles: Object.getOwnPropertyNames(results).join('|'),
          prop: 'imageinfo',
          iiprop: 'url',
          iiurlheight: 180,
          iilimit: 500,
        });
        this.$http.get(tqurl).then(function (response) {

          if (prop != this.selectedProperty || value != this.selectedValue) {
            // other value selected in the meantime, don't process
            return;
          }

          var thumbresults = JSON.parse(response.bodyText).query.pages; 
          var pageindexes = {};
          for (let t in thumbresults) {
            var o = {
              pageid: thumbresults[t].pageid,
              title: thumbresults[t].title,
            };
            if (o.title.endsWith('mp4') || o.title.endsWith('ogv') || o.title.endsWith('webm')) {
              o.isVideo = true;
            } else {
              o.isVideo = false;
            }
            if (thumbresults[t].imageinfo) {
              o.thumburl = thumbresults[t].imageinfo[0].thumburl;
              this.results.push(o);
            } else {
              pageindexes[t] = this.pages.length;
              this.pages.push(o);
              var pageurl = build_url({ 
                format: 'json', 
                action: 'parse', 
                page: o.title,
              });
              this.$http.get(pageurl).then(function (response) {
                var detailData = JSON.parse(response.bodyText).parse;
                var content = get_page_html(detailData);
                this.pages[pageindexes[t]].content = content;
                this.pages[pageindexes[t]].preview = content.substring(0,150);
                // console.log(this.pages[pageindexes[t]].preview);
                this.$forceUpdate();
              });
            }
          }
          if (r['query-continue-offset']) {
            // there are more results, repeat query with indicated offset
            this.selectValue(prop, value, r['query-continue-offset']);
          } else {
            this.status = 'Found ' + this.results.length + ' images and ' + this.pages.length + ' pages.';
          }
          this.$router.replace({query: {prop: this.selectedProperty, value: this.selectedValue}});
        });
      }, function (response) {
        this.status = 'Error ' + response.status + ' while fetching images.';
        // error callback
      });
    },

    selectNextPage: function() {
      this.selectPage(this.nextPage);
    },
    selectPreviousPage: function() {
      this.selectPage(this.previousPage);
    },

    selectPage: function(title, ev) {
      this.selectedPage = title;
      this.detail = {
        title: '',
        imageURL: '',
        content: '',
        preview: '',
        properties: [],
        backlinks: [],
        imagelinks: []
      }

      var result = this.results.filter(r => r.title === title)[0];
      var idx = this.results.indexOf(result);
      var resultArray;
      if (title.startsWith('File:')) {
        resultArray = this.results;
      } else {
        resultArray = this.pages;
      }
      this.nextPage = idx < resultArray.length - 1 ? resultArray[idx + 1].title : resultArray[0].title;
      this.previousPage = idx > 0 ? resultArray[idx - 1].title : resultArray[resultArray.length - 1].title;

      if (ev) {
        $('.quilt-image').parent().removeClass('active-image');
        $('.quilt-video').parent().removeClass('active-image');
        ev.target.classList.add('active-image');
      }

      var txturl = build_url({ 
        action: 'parse', 
        page: title,
      });
      this.$http.get(txturl).then(function (response) {
        var detailData = JSON.parse(response.bodyText).parse;
        this.detail.title = detailData.title;
        this.detail.id = detailData.pageid;
        this.detail.content = get_page_html(detailData);
        this.detail.preview = this.detail.content.substring(0,150);
      });

      if (title.startsWith('File:')) {
        // image url
        var iiurl = build_url({ 
          action: 'query', 
          titles: title,
          prop: 'imageinfo',
          iiprop: 'url',
        });
        this.$http.get(iiurl).then(function (response) {
          var pages = JSON.parse(response.bodyText).query.pages;
          var imgurl = pages[Object.keys(pages)[0]].imageinfo[0].url;
          if (imgurl.endsWith('webm') || imgurl.endsWith('ogv') || imgurl.endsWith('mp4')) {
            this.detail.videoURL = imgurl;
          } else {
            this.detail.imageURL = imgurl;
          }
        });
        // imageusage
        var iuurl = build_url({ 
          action: 'query', 
          list: 'imageusage',
          iutitle: title,
        });
        this.$http.get(iuurl).then(function (response) {
          var iuData = JSON.parse(response.bodyText).query.imageusage;
          this.detail.imagelinks = iuData;
        });
      }

      // backlinks
      var blurl = build_url({ 
        action: 'query', 
        list: 'backlinks',
        bltitle: title,
      });
      this.$http.get(blurl).then(function (response) {
        var blData = JSON.parse(response.bodyText);
      });

      // properties
      var prurl = build_url({ 
        action: 'browsebysubject', 
        subject: title,
      });
      this.$http.get(prurl).then(function (response) {
        var prData = JSON.parse(response.bodyText).query.data;
        var values = [];
        $.each(prData, function(idx, prop){
          if (!prop.property.startsWith('_')) {
            $.each(prop.dataitem, function(i, value) {
              values.push({property: prop.property, value: value.item.replace('#0#', '')});
            });
          }
        });
        this.detail.properties = values;
      });

    },

    getAllProperties: function() {
      var propurl = build_url({ 
        action: 'browsebyproperty', 
      });
      this.$http.get(propurl).then(function (response) {
        var allProps = JSON.parse(response.bodyText).query;
        var userProps = [];
        for (var propname in allProps) {
          if ('isUserDefined' in allProps[propname] && propertiesToIgnore.indexOf(propname) === -1) {
            userProps.push(propname);
          }
        }
        for (var propidx in propertiesToInclude) {
          userProps.push(propertiesToInclude[propidx]);
        }
        this.properties = userProps;
        console.log(this.properties);
      });
    },

    selectProperty: function(prop) {
      this.activeProperty = prop;
      this.activeValues = [];
      // http://smw.referata.com/wiki/List_the_set_of_unique_values_for_a_property
      // e.g. [[Person::+]]|mainlabel=-|headers=hide|?Person|limit=10000
      var valurl = build_url({ 
        action: 'ask', 
        query: '[[' + prop + '::+]]|mainlabel=-|headers=hide|?' + prop + '|limit=10000'
      });
      this.$http.get(valurl).then(function (response) {
        if (this.activeProperty != prop) {
          // first, ensure that user hasn't changed the property in the meantime
          // if changed, don't update the values
          return;
        }
        // create array of unique values
        // clunky but works[tm]
        var r = JSON.parse(response.bodyText);
        var results = r.query.results;
        for (var result in results) {
          var pageValues = results[result].printouts[prop.replace('_', ' ')];
          for (var i in pageValues) {
            var valueInfo = pageValues[i];
            var val;
            if (prop === 'Date') {
              val = valueInfo.raw;
            } else {
              val = valueInfo.fulltext;
            }
            if (this.activeValues.indexOf(val) === -1) {
              this.activeValues.push(val);
            }
          }
        }
        this.activeValues = this.activeValues.sort();
      });
    },
    prevProperty: function() {
      var idx = this.properties.indexOf(this.activeProperty);
      if (idx === 0) {
        idx = this.properties.length;
      }
      this.selectProperty(this.properties[idx-1]);
    },
    nextProperty: function() {
      var idx = this.properties.indexOf(this.activeProperty);
      if (idx === this.properties.length-1) {
        idx = 0;
      }
      this.selectProperty(this.properties[idx+1]);
    },
    

    getValueListClass: function(v) {
      // used for selected item class in value list
      if (v === this.selectedValue) {
        return 'selected-value';
      }
    }
  }
});
