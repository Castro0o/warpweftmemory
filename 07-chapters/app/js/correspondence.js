var correspondenceComponent = {
  template: '#correspondence-template',
  data: function() {
    return {
      letter: {},
      prevLetter: {},
      nextLetter: {},
      allLetters: {},
      allTitles: {}
    };
  },
  
  mounted: function() {
    this.getLetterList();
  },

  methods: {
    getLetterList: function() {
      var qurl = build_url({
        action: 'askargs', 
        conditions: 'TOC::Correspondence'
      });
      var letters = {};
      this.$http.get(qurl).then(function (response) {
        var r = JSON.parse(response.bodyText);
        var results = Object.keys(r.query.results); 

        $.each(results, function(i, title) {
          letters[title] = {
            title: title,
            content: null
          };
          var htmlurl = build_url({
            action: 'parse', 
            page: title,
          });
          vm.$http.get(htmlurl).then(function (response) {
            var detailData = JSON.parse(response.bodyText).parse;
            var content = get_page_html(detailData);
            letters[title].content = content;
          });
        });
        this.allTitles = results;
        this.allLetters = letters;

        this.selectLetter(this.allTitles[0]);
      });
    },
    selectLetter: function(title, ev) {
      this.letter = this.allLetters[title];

      var nextIndex = this.allTitles.indexOf(this.letter.title);
      var nextTitle;
      if (nextIndex == this.allTitles.length-1) {
        nextTitle = this.allTitles[0];
      } else {
        nextTitle = this.allTitles[nextIndex + 1];
      }
      this.nextLetter = this.allLetters[nextTitle];

      var prevIndex = this.allTitles.indexOf(this.letter.title);
      var prevTitle;
      if (prevIndex === 0) {
        prevTitle = this.allTitles[this.allTitles.length-1];
      } else {
        prevTitle = this.allTitles[prevIndex - 1];
      }
      this.prevLetter = this.allLetters[prevTitle];
      preventContextMenu();
    },
    selectNextLetter: function() {
      this.selectLetter(this.nextLetter.title);
    },
    selectPrevLetter: function() {
      this.selectLetter(this.prevLetter.title);
    },
  }
};
