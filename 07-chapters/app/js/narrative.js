var narrativeComponent = {
  template: '#narrative-template',

  data: function() {
    return {
      chapters: [],
      visitedChapters: [],
      all_chapters: [],
      showChapters: true,
      selectedMedia: null,
    };
  },
  props: ['debug'],
  
  mounted: function() {
    this.getChapterList();
    this.loadChapter(starting_chapter, null, true); // load without scrolling
    this.$forceUpdate();
    $('#media-reveal').foundation();
    console.log('narrative is mounted');
  },
  beforeRouteLeave: (to, from, next) => {
    $('.reveal-overlay').remove();
    next();
  },

  methods: {
    loadChapter: function(title, ev, noscroll) {
      if (ev) { ev.preventDefault(); }
      if (!this.showChapters) {
        this.showChapters = true;
      }
      var pageurl = build_url({ 
        action: 'parse', 
        page: title,
      });
      this.$http.get(pageurl).then(function (response) {
        var detailData = JSON.parse(response.bodyText).parse;

        // add this chapter to the DOM
        var chapter = get_chapter_elements(detailData);
        this.chapters.push(chapter);
        this.visitedChapters.push(chapter.title);
        this.$forceUpdate();
        this.$nextTick(function () {
          // wait for next tick for DOM to update and have the elements
          // we've just created
          if (!noscroll) {
            VueScrollTo.scrollTo('.chapter-container:last-child', 500);
          }
        });

        // get properties
        var prurl = build_url({ 
          action: 'browsebysubject', 
          subject: title,
        });
        this.$http.get(prurl).then(function (response) {
          var prData = JSON.parse(response.bodyText).query.data;
          var values = {};
          $.each(prData, function(idx, prop){
            if (!prop.property.startsWith('_') && !prop_exclusions.includes(prop.property)) {
              $.each(prop.dataitem, function(i, value) {
                if (!(prop.property in values)) {
                  values[prop.property] = [];
                }
                values[prop.property].push(value.item.replace('#0#', ''));
              });
            }
          });
          chapter.properties = values;

          // get related images
          var conditions;
          if (!chapter.imageprop1) {
            // no specified image properties in chapter
            var props = Object.getOwnPropertyNames(chapter.properties);
            // console.log(props);
            if (props.length == 1 && props[0] == '__ob__') {
              // console.log("NO PROPERTIES");
              chapter.properties = [];
              return;
            }
            var prop = props[1];
            if (prop === undefined || prop == '__ob__') {
              prop = Object.getOwnPropertyNames(chapter.properties)[0];
            }
            var value = chapter.properties[prop][0]; 
            conditions = prop + '::' + value;
          } else if (!chapter.imageprop2) {
            // only one property
            conditions = '[[' + chapter.imageprop1 + ']]';
          } else {
            // two properties
            conditions = '[[' + chapter.imageprop1 + ']] OR [[' + chapter.imageprop2 + ']]';
          }
          var qurl = build_url({
            action: 'ask', 
            query: conditions + '|limit=500',
          });

          this.$http.get(qurl).then(function (response) {
            var r = JSON.parse(response.bodyText);
            var results = r.query.results; 

            // make an array of the titles of images which have the Herengracht 401::The Photo Archive property
            var rurl = build_url({
              action: 'ask', 
              query: conditions + '[[Herengracht 401::The Photo Archive]]|limit=500',
            });
            this.$http.get(rurl).then(function (response) {
              var r = JSON.parse(response.bodyText);
              var restrictedImages = Object.keys(r.query.results).filter(title => title.startsWith('File:')); 
              // we can only query 50 titles at a time, so do it by chunks
              var titles = Object.getOwnPropertyNames(results);
              while (titles.length) {
                var tqurl = build_url({ 
                  action: 'query', 
                  titles: titles.splice(0, 50).join('|'), // it's here that we remove the titles for the next loop
                  prop: 'imageinfo',
                  iiprop: 'url',
                  iiurlheight: 180,
                  iilimit: 250
                });
                this.$http.get(tqurl).then(function (response) {
                  var thumbresults = JSON.parse(response.bodyText).query.pages; 
                  var pageindexes = {};
                  for (let t in thumbresults) {
                    if (thumbresults[t].imageinfo) {
                      var restricted = false;
                      if (restrictedImages.indexOf(thumbresults[t].title) != -1) {
                        restricted = true;
                      } 
                      var isVideo = false;
                      if (thumbresults[t].title.endsWith('.mov') ||
                          thumbresults[t].title.endsWith('.webm')
                        ) {
                        isVideo = true;
                      }

                      chapter.images.push({
                        pageid: thumbresults[t].pageid,
                        title: thumbresults[t].title,
                        url: thumbresults[t].imageinfo[0].url,
                        thumburl: thumbresults[t].imageinfo[0].thumburl,
                        restricted: restricted,
                        isVideo: isVideo
                      });
                    } else if (thumbresults[t].title.startsWith('File:')) {
                      // Edge case: a few images don't return imageinfo (which
                      // includes thumb data) on a query with multiple titles,
                      // but work on a single query. So we do that here. We get
                      // to this point if the page didn't come with the
                      // imageinfo property.
                      var placeholder = thumbresults[t].title;
                      chapter.images.push(placeholder);
                      iurl = build_url({ 
                        action: 'query', 
                        titles: thumbresults[t].title,
                        prop: 'imageinfo',
                        iiprop: 'url',
                        iiurlheight: 180,
                      });
                      this.$http.get(iurl).then(function (response) {
                        var tresults = JSON.parse(response.bodyText).query.pages; 
                        // FIXME: Duplicated code from above
                        var restricted = false;
                        if (restrictedImages.indexOf(tresults[t].title) != -1) {
                          restricted = true;
                        } 
                        var isVideo = false;
                        if (tresults[t].title.endsWith('.mov') ||
                            tresults[t].title.endsWith('.webm')
                          ) {
                          isVideo = true;
                        }
                        // get index of placeholder and replace it with the image object
                        var idx = chapter.images.indexOf(tresults[t].title);
                        chapter.images[idx] = {
                          pageid: tresults[t].pageid,
                          title: tresults[t].title,
                          url: tresults[t].imageinfo[0].url,
                          thumburl: tresults[t].imageinfo[0].thumburl,
                          restricted: restricted,
                          isVideo: isVideo
                        };
                        this.$forceUpdate;
                      });
                    }
                  }

                  // calculate image count
                  if (chapter.imageprop1) {
                    qurl = build_url({ action: 'askargs', conditions: chapter.imageprop1, parameters: 'limit=500'});
                    this.$http.get(qurl).then(function (response) {
                      var r = JSON.parse(response.bodyText);
                      var results = Object.keys(r.query.results).filter(title => title.startsWith('File:')); 
                      chapter.imageprop1count = results.length;
                    });
                  }
                  if (chapter.imageprop2) {
                    qurl = build_url({ action: 'askargs', conditions: chapter.imageprop2, parameters: 'limit=500'});
                    this.$http.get(qurl).then(function (response) {
                      var r = JSON.parse(response.bodyText);
                      var results = Object.keys(r.query.results).filter(title => title.startsWith('File:')); 
                      chapter.imageprop2count = results.length;
                    });
                  }
                });
              }
              chapter.imagecount = chapter.images.length;

            });
          }, function (response) {
            console.log('Error ' + response.status + ' while fetching images.');
          });


        });

      }, function(error) { 
        console.log('vue error:');
        console.log(error); 
      });
    },
    getChapterList: function() {
      var qurl = build_url({
        action: 'askargs', 
        conditions: 'TOC::Chapter'
      });
      this.$http.get(qurl).then(function (response) {
        var r = JSON.parse(response.bodyText);
        var results = Object.keys(r.query.results); 

        qurl = build_url({
          format: 'json',
          action: 'query', 
          titles: results.join('|'),
          prop: 'revisions',
          rvprop: 'content',
        });
        this.$http.get(qurl).then(function (response) {
          var r = JSON.parse(response.bodyText);
          var results = r.query.pages;
          var chaps = [];
          var total_size = 0;
          $.each(results, function(i, item) {
            total_size += item.revisions[0]['*'].length;
          });
          var size_factor = 100 / total_size;
          $.each(results, function(i, item) {
            var size = item.revisions[0]['*'].length;
            chaps.push({
              title: item.title,
              size: size * size_factor
            });
          });
          this.all_chapters = chaps.sort(function(a, b) {
            // sort by title
            var nameA = a.title.toUpperCase(); // ignore upper and lowercase
            var nameB = b.title.toUpperCase();
            if (nameA < nameB) {
              return -1;
            }
            if (nameA > nameB) {
              return 1;
            }
            // names must be equal
            return 0;
          });
          preventContextMenu();
        });
      });

    },
    selectChapter: function(ev) {
      this.loadChapter(ev.target.value);
    },
    selectMedia: function(img) {
      if (this.showChapters) {
        // only select if we're in the image layer
        return;
      }

      this.selectedMedia = null;
      this.selectedMedia = img;
      // get image description
      var pageurl = build_url({ action: 'parse', page: img.title });
      this.$http.get(pageurl).then(function (response) {
        var detailData = JSON.parse(response.bodyText).parse;
        img.description = get_page_html(detailData);

        // get image properties
        var pageurl = build_url({ action: 'browsebysubject', subject: img.title });
        this.$http.get(pageurl).then(function (response) {
          var prData = JSON.parse(response.bodyText).query.data;
          var values = {};
          $.each(prData, function(idx, prop){
            if (!prop.property.startsWith('_')) {
              $.each(prop.dataitem, function(i, value) {
                if (!(prop.property in values)) {
                  values[prop.property] = [];
                }
                values[prop.property].push(value.item.replace('#0#', ''));
              });
            }
          });
          img.properties = values;
          // console.log(Vue.plain(this.selectedMedia));
          // this.selectedMedia = img;
          this.$forceUpdate();
          $('#media-reveal').foundation('open');
        });
      });
      // stop video when closing sidebar
      $(document).on(
        'closed.zf.reveal', '[data-reveal]', function () {
          if ($('#detail-video').length) {
            $('#detail-video')[0].pause();
            console.log('video closed');
          }
        }
      );
    },
    toggleChapterMode: function(ev) {
      if (
          (ev.target.tagName == "SECTION" && ev.target.classList.contains("chapter-sublayer")) ||
          (ev.target.tagName == "UL" && ev.target.classList.contains("image-grid")) ||
          (this.showChapters && (ev.target.tagName == "LI" && ev.target.classList.contains("image"))) ||
          (this.showChapters && (ev.target.tagName == "IMG" && ev.target.classList.contains("chapter-media"))) ||
          (!this.showChapters && (ev.target.classList.contains("chapter-header"))) 
        ) { 
        this.showChapters = !this.showChapters;
        // close the image detail modal
        $('#media-reveal').foundation('close');
      }
    },
    chapterClass: function (chapter) {
      return 'align-' + Math.floor(Math.random()*4+1);
    },
    isParaRestricted: function(para) {
      if (para.includes('"restricted"')) {
        return true;
      } else {
        return false;
      }
    },
  }, 
};
