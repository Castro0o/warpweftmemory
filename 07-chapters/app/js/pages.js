var colophonComponent = {
  template: '#colophon-template',
  data: function() {
    return {
      colophonContent: null,
    };
  },
  
  mounted: function() {
    this.getPageContent('Colophon');
  },

  methods: {
    getPageContent: function(name) {
      var htmlurl = build_url({
        action: 'parse', 
        page: name
      });
      this.$http.get(htmlurl).then(function (response) {
        var detailData = JSON.parse(response.bodyText).parse;
        var content = get_page_html(detailData, true);
        this.colophonContent = content;
      });
    },
  }
};

// duplicate code, but I don't have time to do better now
var forewordComponent = {
  template: '#foreword-template',
  data: function() {
    return {
      forewordContent: null,
    };
  },
  
  mounted: function() {
    this.getPageContent('Foreword');
  },

  methods: {
    getPageContent: function(name) {
      var htmlurl = build_url({
        action: 'parse', 
        page: name
      });
      this.$http.get(htmlurl).then(function (response) {
        var detailData = JSON.parse(response.bodyText).parse;
        var content = get_page_html(detailData, true);
        this.forewordContent = content;
      });
    },
  }
};

