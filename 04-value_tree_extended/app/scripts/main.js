
var base_url = 'http://37.218.246.155/warpweft-wiki/'; // use trailing slash plz
var apiurl = base_url + 'api.php';

// http://stackoverflow.com/a/1026087
String.prototype.capitalize = function() {
  return this.charAt(0).toUpperCase() + this.slice(1);
};


function build_url(querydict) {
  // helper function to construct the URL for the API query
  // var qurl = build_url({ format: 'json', action: 'query', 'titles': 'Property:Person' });
  if ( !querydict ) { return apiurl; }
  var url = apiurl + '?';
  $.each(querydict, function(key, value) {
    if ( url.substr(-1) != '?') {
      url += '&';
    }
    url += key + '=' + encodeURIComponent(value); 
  });
  return url;
}

function slugify(s) {
  // helper function to give us nice slugs
  return s.toLowerCase().replace(/ /g, '-').replace("'", "").replace("è", "e").replace(".", "");
}


function fill_related_links(prop, value, offset) {
  // Fetch and show the pages for a specific property-value (e.g. Person / Gisèle)
  // api.php?format=json&action=askargs&conditions=person%3A%3A%2B&printouts=Person
  offset = typeof offset !== 'undefined' ? offset : 0;
  var prop_slug = slugify(prop);
  var value_slug = slugify(value);
  var qurl = build_url({ format: 'json', 
    action: 'askargs', 
    conditions: prop + '::' + value, 
    parameters: 'offset=' + offset
  });

  //$.get(qurl, function (data, e) {
    //if ( e != "success" ) { 
      //console.log("Errors found in ask query:"); 
      //console.log(e);
    //} else {
  $.ajax({
      url: qurl,
      type: 'GET',
      dataType: 'json',
      success: function (data, e) {

      var count = 1;
      $.each(data.query.results, function(i, r) {
        // find the appropriate div for this page's property values, and place it there
        $('#property-' + prop_slug + '-value-' + value_slug + '-list ul.related-items-list').append('<li id="related-' + prop_slug + '-' + value_slug + '-' + count + '" class="related-items-item">');
        var $li = $('#related-' + prop_slug + '-' + value_slug + '-' + count);
        var thumbtext = r.fulltext.replace('File:', '').replace('.jpg', '').replace('.png', '');
        $li.append('<a class="page-name" href="' + r.fullurl + '">' + thumbtext + '</a>');

        var title = r.fulltext;
        var tqurl;
        if (title.startsWith('File:')) {
          tqurl = build_url({ 
            format: 'json', 
            action: 'query', 
            titles: title,
            prop: 'imageinfo',
            iiprop: 'url',
            iiurlheight: 250
          });
          $.ajax({
              url: tqurl,
              type: 'GET',
              dataType: 'json',
              success: function (data, e) {
            var imginfo = data.query.pages[Object.keys(data.query.pages)[0]].imageinfo[0];
            $li.append('<span class="image-preview" id="' + 'preview-' + prop_slug + '-' + value_slug + '-' + count + '"><img src="' + imginfo.thumburl + '" alt="' + title + '"></span>');
              }
          });
        } else {
          tqurl = build_url({ 
            format: 'json', 
            action: 'parse', 
            page: title,
          });
          $.ajax({
              url: tqurl,
              type: 'GET',
              dataType: 'json',
              success: function (data, e) {
            var contents = data.parse.text['*'];
            $li.append('<span class="page-preview" id="' + 'preview-' + prop_slug + '-' + value_slug + '-' + count + '">' + contents + '</span>');
              }
          });
        }
        // previews
        // add_preview_content(prop, value, count, r.fulltext);

        count++;
      }); 
    if (data['query-continue-offset']) {
      get_values_for_property(prop, value, data['query-continue-offset']);
    }
      }
  });
}



function add_preview_content(prop, value, count, title) {
  // Add the thumbnail or page content to the appropriate div
  var prop_slug = slugify(prop);
  var value_slug = slugify(value);
  var $prev = $('<div id="preview-' + prop_slug + '-' + value_slug + '-' + count + '" class="page-preview"></div>').appendTo('#property-' + prop_slug + '-value-' + value_slug + '-list');
  $prev.hide();

  var qurl;
  if (title.startsWith('File:')) {
    qurl = build_url({ 
      format: 'json', 
      action: 'query', 
      titles: title,
      prop: 'imageinfo',
      iiprop: 'url',
      iiurlheight: 250
    });
    $.ajax({
        url: qurl,
        type: 'GET',
        dataType: 'json',
        success: function (data, e) {
      var imginfo = data.query.pages[Object.keys(data.query.pages)[0]].imageinfo[0];
      $prev.append('<img src="' + imginfo.thumburl + '" alt="' + title + '">');
        }
    });
  } else {
    qurl = build_url({ 
      format: 'json', 
      action: 'parse', 
      page: title,
    });
    $.ajax({
        url: qurl,
        type: 'GET',
        dataType: 'json',
        success: function (data, e) {
          var contents = data.parse.text['*'];
          $prev.append(contents);
      }
    });
  }

  var prev_id = $prev[0].id;
  var related_id = $prev[0].id.replace('preview-', 'related-');
  var $rel = $('#' + related_id + ' a');
  $rel.hover(function() {
    if (!$($prev).is(':visible')) {
      $prev.show('fast');     
    }
  });
  $rel.on('mouseout', function() {
    if ($prev.is(':visible')) {
      $prev.hide();     
    }
  });
}


function parse_results_for_values(prop, results) {
  // Process and display the values for each property
  $.each(results, function(idx, result) {
    var prop_slug = slugify(prop);

    $.each(result.printouts[prop.capitalize()], function(i, v) {
      if (v.fulltext) {
        var value = v.fulltext;
        prop = prop.toLowerCase();
        var value_slug = slugify(value);
        // create list of values
        if (!$('li#property-' + prop_slug + '-value-' + value_slug).length) {
          var $li = $('<li id="property-' + prop_slug + '-value-' + value_slug + '"></li>').appendTo('#property-' + prop_slug + ' ul.value-items');
          $li.append('<a href="#/">' + value + '</a>');

          // fill the hidden value containers
          var $valuebox = $('<div id="property-' + prop_slug + '-value-' + value_slug + '-list" class="property-value-list row"></div>').appendTo('#property-' + prop_slug + ' #value-list-container');

          $valuebox.append('<p id="align-list" class="utility small-12 columns"><a href="' + v.fullurl + '" target="_blank">Go to <strong>' + value + '</strong> →</a></p>');
          $valuebox.append('<div class="small-12 columns"><ul class="related-items-list no-bullet small-block-grid-2 medium-block-grid-5 large-block-grid-7"></ul></div>');
          fill_related_links(prop, value);  

          // $(document).foundation('equalizer', 'reflow');

          $('#property-' + prop + '-value-' + value_slug).click(function(e) {
              e.preventDefault();
              var li_id = $(this)[0].id;
              //console.log(li_id + '-list');
      
              // close open property-list if you click in another property or place
              $('.property-value-list').not('#'+ li_id + '-list').hide();
             
              // remove class active if you click in another property
              $('.active').not(this).removeClass('active');
              $(this).toggleClass('active');
              $('#'+ li_id + '-list').toggle('fast');
              
              // vertical align related items list
              var myid = $('#'+ li_id);
              
              /* https://stackoverflow.com/questions/17218765/jquery-get-top-offset-relative-to-a-specific-div#17218812
              */
              var myoffset = myid.offset().top - $('#property-' + prop).offset().top - myid.outerHeight(true);
              var myvalue  = myoffset + 'px';
              console.log(myvalue);
              
              $('#'+ li_id + '-list #align-list').css('marginTop', myvalue);
              return false;  
          });          
        }
      } 
    });
  });
}


function get_values_for_property(prop, offset) {
  // Fetch the values for each property and pass them on to parse_results_for_values
  // api.php?format=json&action=askargs&conditions=person%3A%3A%2B&printouts=Person
  offset = typeof offset !== 'undefined' ? offset : 0;
  var qurl = build_url({ format: 'json', 
    action: 'askargs', 
    conditions: prop + '::+', 
    printouts: prop.capitalize(),
    parameters: 'offset=' + offset
  });
  $.ajax({
    url: qurl,
    type: 'GET',
    dataType: 'json',
    success: function (data, e) {
      parse_results_for_values(prop, data.query.results);
      if (data['query-continue-offset']) {
        get_values_for_property(prop, data['query-continue-offset']);
      }
    }
  });
}



function make_ask_query(q, property, offset) {
  // helper function to make an API query
  // default offset should be 0
  offset = typeof offset !== 'undefined' ? offset : 0;
  var qurl = build_url({ format: 'json', 
    action: 'askargs', 
    conditions: q, 
    printouts: property.capitalize(),
    parameters: 'offset=' + offset
  });
  var results = [];
  $.ajax({
    url: qurl,
    type: 'GET',
    dataType: 'json',
    success: function (data, e) {
      add_to_results(data, property);
      if (data['query-continue-offset']) {
        var more_results = make_ask_query(q, property, data['query-continue-offset']);
        results.push.apply(results, more_results);
      }
    }
  });
}

$(document).ajaxStop(function() {
  $('#content-loader').hide('slow');
});

$(document).ready(function(){
  $.each(['person', 'object', 'place', 'date', 'medium', 'register', 'creatures'], function(idx, prop) {
    // Add property section
    $('<div id="property-' + prop + '" class="property row"></div>').appendTo('#main-container');
    $('<div class="small-2 medium-1 columns"></div>').appendTo('#property-' + prop);
    $('<div class="property-name"><h4>' + prop.capitalize() + '</h4></div>').appendTo('#property-' + prop + ' div');

    $('<div id="property-' + prop + '-list" class="property-list small-3 medium-3 columns"></div>').appendTo('#property-' + prop);
    $('<div id="value-list-container" class="small-7 medium-8 end columns"></div>').appendTo('#property-' + prop);
    $('<ul class="no-bullet value-items"></ul>').appendTo('#property-' + prop + '-list');
    get_values_for_property(prop);
    
  });
});
